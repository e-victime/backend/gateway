# CHANGELOG

<!--- next entry here -->

## 1.3.4
2020-05-26

### Fixes

- **discovery:** Change zone URL (b057255604fa7b4a97628278dbb9b9546e27817b)

## 1.3.3
2020-05-25

### Fixes

- **discovery:** Fix missing port on Eureka URL (92f99569c48594147513c0b63ed4ac02687d38d9)

## 1.3.2
2020-05-20

### Fixes

- **routes:** Repair routes on Gateway (9fc97e9258a99f95e39871c391e7ebfeb59df437)

## 1.3.1
2020-05-20

### Fixes

- **external:** fix external api access (42c6822b564ffd108a589cfafeb91458db58ed28)

## 1.3.0
2020-05-19

### Features

- **api:** Add swagger-ui doc (d3f833b4f85a465bfbad7680aba6b62b772d22f2)
- **gateway:** serve swagger-ui (8b7e2bb11f9560dd8861edaaeb5c910d4109304f)

### Fixes

- **cors:** Add cors on nginx ingress (a4d629cf4594b68810be395fa42899bc64324fff)

## 1.2.1
2020-05-12

### Fixes

- **routes:** Change micro-services url (58b27819145689f343fbac24f443b0cd8bace5c2)

## 1.2.0
2020-05-11

### Features

- **ci:** Add tests on maven package (395fd8fc4b00e33edb22657b6762d621e66c13b6)

## 1.1.0
2020-05-11

### Features

- **env:** Add other micro-service hostname in env var (efb6fac39256e5e7602addf147002dd9c12930c1)
- **deploy:** Go on kubernetes (e001ec90efa2d3d20a41a8079734b02484fdde77)

### Fixes

- **config:** Add discovery config (cc296d4d03b086197866116bbd904f50231060c3)
- **deployment:** Change registry url (a990c873fe7699974d8be9073991fb0b3ccf6573)
- **import:** Cleanup import (b06e1ac7ac9417dc139221500941bc7c05460760)
- **routes:** Add routes (5d03d6e8f22e9c8defecfc2215d4276c2a040c87)
- **ci:** Repair changelog for ci (8135a133139934d5979799a779607de4e55aab7d)

## 1.0.0
2020-05-09

### Features

- **backend:** Init micro-service & Add connexion to discovery (a128e2dab9e684f29820e4b76f908d555dbfb57e)

### Fixes

- **ci:** add settings for deployment in gitlab (268f47c5c24b49a89998e110be8d465d41344370)
- **ci:** add settings for deployment in gitlab (dd311f7381e923aaef8de845eb2c03bf36571607)
- **config:** Add discovery config (cc296d4d03b086197866116bbd904f50231060c3)
- **deployment:** Change registry url (a990c873fe7699974d8be9073991fb0b3ccf6573)